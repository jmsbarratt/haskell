import System.Random
import Data.Char

begin :: String -> Int -> IO()
begin _ 0 = do
  putStrLn "Give youself a chance. You need more than 0 lives to get started!"
begin file turns = do
  file_content <- readFile file
  let file_lines = lines file_content
  if ((length file_lines) > 0) then do
    random_index <- randomInt 0 (length file_lines)
    let word = map toLower (file_lines !! random_index)
    starman word (mask word '-') [] turns
  else
    putStr "No lines found in the input file."

randomInt :: Int -> Int -> IO Int
randomInt min max = do
  gen <- newStdGen
  return (head (randomRs (min, max) gen))

mask :: String -> Char -> String
mask string mask_char = take (length string) (repeat mask_char)

starman :: String -> String -> [Char] -> Int -> IO()
starman word _ _ 0 = do
  putStrLn ("You lose. The answer was: " ++ word)
starman word display guesses n = do
  putStrLn ("Stars: " ++ take n (repeat '*'))
  putStrLn ("Guesses: " ++ guesses)
  putStrLn display
  putStr "Enter your guess: "
  guess <- getLine
  let guesses' = addGuess (map toLower (filter (\x -> x `elem` ['a'..'z']) guess)) guesses
  if (guesses /= guesses') then do
    let display' = unmask word display (last guesses')
    if (word == display') then
      putStrLn ("You win! The answer was: " ++ word)
    else
      starman word display' guesses' (if (display' /= display) then n else n-1)
  else
    starman word display guesses (n-1)

addGuess :: String -> String -> String
addGuess [] guesses = guesses
addGuess guess guesses =
  if ((last guess) `elem` guesses) then
    addGuess [] guesses
  else
    guesses ++ [last guess]

unmask :: String -> String -> Char -> String
unmask word display c = [if x==c then x else y | (x,y) <- zip word display]